# participatedb-to-yaml

Convert ParticipateDB web site to a Git repository of YAML files.


## Usage

```bash
virtualenv --python=python3 v
source v/bin/activate
pip install -r requirements.txt
./harvest_participatedb.py html/
./participatedb_to_yaml.py html/ ../participatedb-yaml/
```

## Development

```bash
pip install tox
tox
```
